# Tidsbanken Vacation Planner

## Live version
Live version is hosted at: https://tidsbanken.herokuapp.com/

## Collaborators
@jakobah37 Jakob Henriksen

@KPed Kasper Pedersen

@kaspernie Kasper Nielsen

@Nsknielsen Nikolaj Nielsen

## How to run a local version
Clone this repository.

Open a terminal in the repository root folder and type:

```npm install```

```npm start```


Be aware that a local version will not work without reconfiguration to use a local instance of the identity provider (https://www.keycloak.org/) and backend with associated PostGres database (https://gitlab.com/KPed/tidsbanken-api), as the live versions of all 3 programs are configured to allow CORS requests from each other's URLs.
