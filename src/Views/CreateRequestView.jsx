import React from "react";
import CreateRequestForm from "../Components/CreateRequest/CreateRequestForm";

const CreateRequestView = () => {
  return (
    <div className="dv-createrequestview">
      <CreateRequestForm></CreateRequestForm>
    </div>
  );
};

export default CreateRequestView;
