import { useKeycloak } from "@react-keycloak/web";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { addBackendUser, addKeycloakUser } from "../../api/adminArea";
import "../../forms.css";
import "../../buttons.css";

const CreateUserForm = ({ inputLengthConfig, emailInputLengthConfig }) => {
  //using function from react hook form so we can handle the form in an SPA way without reloading page on submit
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({ mode: "onChange" });

  //local state to hold information about whether creating user was successful
  const [createUserResponse, setCreateUserResponse] = useState(null);

  // get keycloak jwt (token)
  const { keycloak, initialized } = useKeycloak();
  //our submit handler function to create a user with password on Keycloak and BE
  const onSubmit = async ({
    firstName,
    lastName,
    email,
    username,
    password,
    isAdmin,
  }) => {
    const userObject = { firstName, lastName, email, username };
    const [keycloakResponse, keycloakError] = await addKeycloakUser(
      userObject,
      password,
      isAdmin
    );
    if (keycloakResponse) {
      //get new userId and add to object for storage in backend too
      const userUrl = keycloakResponse.headers.get("location");
      const userId = userUrl.split("users/")[1];
      userObject.id = userId;
      const [backendResponse, backendError] = await addBackendUser(
        userObject,
        isAdmin,
        keycloak.token
      );
      if (backendResponse) {
        setCreateUserResponse("User created successfully");
      } else {
        setCreateUserResponse(backendError);
      }
    } else {
      setCreateUserResponse(keycloakError);
    }
  };

  return (
  <div>
          <h2>Create a new user</h2>
          <form onSubmit={handleSubmit(onSubmit)}>
            <fieldset>
              <label htmlFor="firstName">First name:</label>
              <input
                className="form-control"
                type="text"
                {...register("firstName", inputLengthConfig)}
                placeholder="Limit 40 characters"
              />
            </fieldset>
            <fieldset>
              <label htmlFor="lastName">Last name:</label>
              <input
                className="form-control"
                type="text"
                {...register("lastName", inputLengthConfig)}
                placeholder="Limit 40 characters"
              />
            </fieldset>
            <fieldset>
              <label htmlFor="email">Email:</label>
              <input
                className="form-control"
                type="text"
                {...register("email", emailInputLengthConfig)}
                placeholder="Limit 100 characters"
              />
            </fieldset>
            <fieldset>
              <label htmlFor="username">Username:</label>
              <input
                className="form-control"
                type="text"
                {...register("username", inputLengthConfig)}
                placeholder="Limit 40 characters"
              />
            </fieldset>
            <fieldset>
              <label htmlFor="password">Temporary password:</label>
              <input
                className="form-control"
                type="password"
                {...register("password", inputLengthConfig)}
                placeholder="Limit 40 characters"
              />
            </fieldset>
            <fieldset>
              <label htmlFor="isAdmin">Is this user an administrator?</label>
              <div className="form-check">
                <input
                    className="form-check-input"
                    type="checkbox"
                    {...register("isAdmin")}
                />
              </div>

            </fieldset>
            <button className="btn-logo btn-sep btn-logo-logo icon-adduser" type="submit" disabled={!isValid}>
              Create user
            </button>
          </form>
          {createUserResponse}
        </div>
  );
};

export default CreateUserForm;
