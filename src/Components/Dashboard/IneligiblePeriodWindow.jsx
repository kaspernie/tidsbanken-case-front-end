import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css"; //necessary CSS to actually display a modal window
import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid"; // plugin for dayGridMonth view
import interactionPlugin from "@fullcalendar/interaction"; // plugin for selectable
import { useSelector } from "react-redux";
import { useKeycloak } from "@react-keycloak/web";
import {
  addIneligiblePeriod,
  deleteIneligibleById,
} from "../../api/ineligible";
import { Button } from "react-bootstrap";

//modal window where admins can add/delete  ineligible periods, coming from the dashboard
const IneligiblePeriodWindow = ({
  modalIsOpen,
  toggleModalWindow,
  handleLoadIneligibles,
}) => {
  //local state storing the current calender selection
  const [selection, setSelection] = useState(null);

  //local state for checking if modal window for deleting ineligibles should be open, and which ineligible to delete by id
  const [addModalIsOpen, setAddModalIsOpen] = useState(false);
  const [deleteModalIsOpen, setDeleteModalIsOpen] = useState(false);
  const [deleteIneligibleId, setDeleteIneligibleId] = useState(null);

  //get ineligibleperiods using redux
  const ineligibleListState = useSelector((state) => state.ineligiblePeriods);

  //handler for when user selects a section of the calendar
  const handleSelectDates = (selectionInfo) => {
    setSelection(selectionInfo);
  };

  //keycloak access to get token
  const { keycloak, initialized } = useKeycloak();

  //handler for adding a new ineligible period
  const handleAddIneligiblePeriod = async () => {
    const token = keycloak.token;
    const [newIneligible, error] = await addIneligiblePeriod(token, selection);
    toggleAddWindow();
    handleLoadIneligibles();

  };

  //show/hide delete-ineligible-window
  const toggleDeleteWindow = () => {
    setDeleteModalIsOpen(!deleteModalIsOpen);
  };

  const toggleAddWindow = () => {
    setAddModalIsOpen(!addModalIsOpen);
  };

  //handler for when admins click an ineligible to delete it
  const handleIneligibleEventClick = (eventClickInfo) => {
    toggleDeleteWindow();
    setDeleteIneligibleId(eventClickInfo.event.id);
  };

  //actually delete an ineligible when user confirms this is their intent
  const handleDeleteIneligible = async () => {
    const token = keycloak.token;
    const [deleteIneligible, error] = await deleteIneligibleById(
      token,
      deleteIneligibleId
    );
    toggleDeleteWindow();
    handleLoadIneligibles();
  };

  return (
    <div>
      <Modal show={modalIsOpen} size="lg">
        <Modal.Header closeButton onHide={toggleModalWindow}>
          <Modal.Title>Ineligible Periods</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Select dates to add a new ineligible period or
          click on an existing period to delete it.</p>
          <Modal show={deleteModalIsOpen} centered>
            <Modal.Header closeButton onHide={toggleDeleteWindow}>
              <Modal.Title>
                Delete Ineligible Period with id {deleteIneligibleId}?
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <button className={"btn-logo btn-sep btn-logo-logo icon-delete"} onClick={handleDeleteIneligible}>
                Delete
              </button>
            </Modal.Body>
          </Modal>
          <Modal show={addModalIsOpen} centered>
            <Modal.Header closeButton onHide={toggleAddWindow}>
              <Modal.Title>
                Add Ineligible Period?
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <h5>Dates selected:</h5>
              <p>
                {selection && (
                    [selection.start.toLocaleString('dk-DK',{ weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }),
                      " to ",
                      new Date(selection.end-1).toLocaleString('dk-DK',{ weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })])}
              </p>
              <button className={"btn-logo btn-sep btn-logo-logo icon-check"} onClick={handleAddIneligiblePeriod}>
                Add
              </button>
            </Modal.Body>
          </Modal>
          <FullCalendar
            plugins={[dayGridPlugin, interactionPlugin]}
            headerToolbar={{ left: "title", center: "", right: "prev,next" }}
            initialView="dayGridMonth"
            height={600}
            events={ineligibleListState}
            selectable={true}
            unselectAuto={false}
            select={handleSelectDates}
            eventClick={handleIneligibleEventClick}
          ></FullCalendar>
        </Modal.Body>
        <Modal.Footer>
          <button className={"btn-logo btn-sep btn-logo-logo icon-close"} onClick={toggleModalWindow}>
            Close
          </button>
          <button className={"btn-logo btn-sep btn-logo-logo icon-check"} onClick={toggleAddWindow}>
            Add
          </button>

        </Modal.Footer>
      </Modal>

    </div>
  );
};

export default IneligiblePeriodWindow;
