import { useKeycloak } from "@react-keycloak/web";
import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  getNotifications,
  updateNotificationContentStatus,
} from "../../api/notifications";
import {
  notificationTypeComment,
  notificationTypeRequest,
} from "../../const/notificationTypes";
import "../../toast.css";

const NotificationComponent = () => {
  //router access to navigate when toasts are clicked
  const navigate = useNavigate();

  //shows toasts on mount
  useEffect(() => {
    loadNotifications();
  }, []);

  //keycloak access to get token
  const { keycloak, initialized } = useKeycloak();

  //load relevant requests and comments that have been edited since last logout, using userId to get the right ones
  const loadNotifications = async () => {
    const token = keycloak.token;
    const userId = keycloak.tokenParsed.sub;
    const [loadedNotifications, error] = await getNotifications(token, userId);
    console.log(loadedNotifications)
    if (loadedNotifications) {
      showNotifications(loadedNotifications);
    }
  };

  //show notifications in gui
  const showNotifications = async (notifications) => {
    //make notif toasts
    for (const notification of notifications) {
      toast.info(() => makeRequestNotificationMsg(notification), {
        newestOnTop: true,
        position: toast.POSITION.BOTTOM_RIGHT,
        icon: <img src="/logo_32.png" style={{ width: "24px" }} />,
        className: "french-toast",
        bodyClassName: "french-toast-body",
      });
    }
  };

  //jsx of the message to show as a notification
  const makeRequestNotificationMsg = (notification) => (
    <div onClick={() => handleClickNotification(notification)}>
      {notification.type === notificationTypeRequest ? (
        <div>
          New activity on <i>{notification.content.title}</i>. Click to go to
          request.
        </div>
      ) : (
        <div>
          New comment: <i>{notification.content.message.slice(0, 15)}... </i>
          Click to go to request.
        </div>
      )}
    </div>
  );

  //navigating to the request in a notification
  const handleClickNotification = async (notification) => {
    let requestId;
    if (notification.type === notificationTypeComment) {
      requestId = parseInt(
        notification.content.request.split("/api/request/")[1]
      );
    } else {
      requestId = notification.content.id;
    }
    //patch api call to backend that sets seen:true for this notification's content
    const token = keycloak.token;
    const [updateStatusResponse, error] = await updateNotificationContentStatus(
      token,
      notification.type,
      notification.content.id,
      true
    );
    console.log("patch resp", updateStatusResponse, error);
    //navigate to the appropriate request
    navigate(`/request/${requestId}`);
  };

  return (
    <div>
      <ToastContainer autoClose={false} limit={4} />
    </div>
  );
};

export default NotificationComponent;
