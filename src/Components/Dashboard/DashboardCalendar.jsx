import React, { useEffect, useState } from "react";
import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid"; // plugin for dayGridMonth view
import { useNavigate } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css"; //necessary CSS to actually display a modal window
import "../../calendar.css";
import "../../buttons.css";
import { useKeycloak } from "@react-keycloak/web";
import { getRequests } from "../../api/request";
import { useSelector } from "react-redux";
import {
  REQUEST_STATUS_APPROVED,
  REQUEST_STATUS_DENIED,
  REQUEST_STATUS_PENDING,
} from "../../const/requestStatus";
import {
  COLOR_REQUEST_APPROVED,
  COLOR_REQUEST_DENIED,
  COLOR_REQUEST_PENDING,
} from "../../const/colors";

//component for holding our visual FullCalendar and populating it with data from api and redux state
const DashboardCalendar = ({ handleLoadIneligibles }) => {
  //use react router to navigate to request details page
  const navigate = useNavigate();

  //handler for clicking a scheduled vacation event. TODO post-MVP navigate to the details page for that event.
  const handleEventClick = (eventClickInfo) => {
    if (!eventClickInfo.event.extendedProps.isIneligible) {
      //create url based on request id and navigate to its details page
      const requestUrl = "/request/" + eventClickInfo.event.id;
      navigate(requestUrl);
    }
  };

  //local state for showing reqeust fetching errors
  const [loadRequestsError, setLoadRequestsError] = useState(null);

  //keycloak access to get token
  const { keycloak, initialized } = useKeycloak();

  //local state for requests
  const [requests, setRequests] = useState([]);

  //useEffect to get requests on mount (empty dependency array)
  useEffect(() => {
    handleLoadRequests();
    handleLoadIneligibles();
  }, []);

  // get calender color from request status (pending, approved)
  const getCalendarEventColor = (requestStatus) => {
    let color = null;
    switch (requestStatus) {
      case REQUEST_STATUS_PENDING:
        color = COLOR_REQUEST_PENDING;
        break;
      case REQUEST_STATUS_APPROVED:
        color = COLOR_REQUEST_APPROVED;
        break;
      case REQUEST_STATUS_DENIED:
        color = COLOR_REQUEST_DENIED;
        break;
      default:
        color = "pink";
    }
    return color;
  };

  //handle loading all requests on mount
  const handleLoadRequests = async () => {
    const token = keycloak.token;
    let pageNumber = 0;
    let requestsPaginationObject;
    let error;
    do {
      [requestsPaginationObject, error] = await getRequests(token, pageNumber);
      if (requestsPaginationObject.content) {
        //reformat requests to fit calendar needs
        const pageOfCalendarRequests = requestsPaginationObject.content.map(
          (request) => formatCalendarRequest(request)
        );

        //add this page of requests to the existing local request state
        const newTotalRequests = [...requests, ...pageOfCalendarRequests];
        setRequests(newTotalRequests);

        //update page number in case we need to paginate further
        pageNumber += 1;
      } else {
        setLoadRequestsError(error);
      }
    } while (!requestsPaginationObject.last);
  };

  //FORMATTER for converting backend data to Fullcalendar format
  const formatCalendarRequest = (request) => {
    return {
      title: `${request.user.firstName} ${request.user.lastName}:  ${request.title}`,
      start: request.periodStart,
      end: request.periodEnd,
      id: request.id,
      backgroundColor: getCalendarEventColor(request.requestStatus),
    };
  };

  //redux state access to ineligibles
  const ineligibles = useSelector((state) => state.ineligiblePeriods);

  return (
    <div>
      {loadRequestsError}
      {ineligibles && (
        <FullCalendar
          headerToolbar={{ left: "", center: "prev,title,next", right: "" }}
          themeSystem="bootstrap5"
          //remember to register all plugins here
          plugins={[dayGridPlugin]}
          initialView="dayGridMonth"
          //weekends={false} - consider using this since it is a work holiday scheduler, or rather making weekends excluded from any event spans
          //list of existing events
          events={[...requests, ...ineligibles]}
          height={700}
          displayEventTime={false}
          //what to do when an event is clicked
          eventClick={handleEventClick}
          //max nr of events to show on a day, rest shown in popover - ugly base implementation
          dayMaxEventRows={3}
        />
      )}
    </div>
  );
};

export default DashboardCalendar;
