import React from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";

//Button to navigate to new request view
const CreateRequestButton = () => {
  //access navigation functionality from router module
  const navigate = useNavigate();

  //click handler to navigate to new request view.
  const handleClick = () => {
    navigate("/create/request");
  };

  return (
    <>
      <button className={"btn-logo btn-sep btn-logo-logo icon-paperplane"} onClick={handleClick}>Create new vacation request</button>
    </>
  );
};

export default CreateRequestButton;
