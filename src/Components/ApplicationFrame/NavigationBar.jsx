import { useKeycloak } from "@react-keycloak/web";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import { updateLogoutTimestamp } from "../../api/profile";
import { USER_ROLE_ADMIN } from "../../const/userRoles";
import "../../index.css";
import "../../navbar.css";

//Navbar
const NavigationBar = () => {
  //gets access to keycloak info in order to show/hide navigation links based on user login and type
  const { keycloak, initialized } = useKeycloak();

  return (
    <>
      <Navbar className="my-nav" expand="lg">
        <Container>
          <Navbar.Brand as={Link} to="/">
            <img
              alt="TB"
              src="/logo_32.png"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{" "}
          </Navbar.Brand>
          {/* shows navbar + username + logout component if user is logged in */}
          {keycloak.authenticated && (
            <>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav variant="tabs" className="me-auto">
                  <Nav.Link as={Link} to="/">
                    Calendar
                  </Nav.Link>
                  {/* only shows the admin settings if user is admin */}
                  {keycloak.tokenParsed.roles.includes(USER_ROLE_ADMIN) && (
                    <Nav.Link as={Link} to="/admin">
                      Admin area
                    </Nav.Link>
                  )}
                  <NavDropdown
                    title={"Profile"}
                    id="profile-dropdown"
                  >
                    <NavDropdown.Item as={Link} to="/user/history">
                      Vacation Request History
                    </NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/user/profile">
                      Update Profile Details and Password
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item onClick={keycloak.logout}>
                      Logout
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav>
              </Navbar.Collapse>
            </>
          )}
        </Container>
      </Navbar>
    </>
  );
};

export default NavigationBar;
