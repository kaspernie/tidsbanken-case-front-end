import { useKeycloak } from "@react-keycloak/web";
import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import { useSelector } from "react-redux";
import { updateRequestDetails } from "../../api/request";
import BaseRequestForm from "../../Helpers/BaseRequestForm";
import "bootstrap/dist/css/bootstrap.min.css";

const EditRequestForm = ({
  modalIsOpen,
  toggleModalWindow,
  requestId,
  updateRequestInfoState,
}) => {
  //keycloak access to conditionally render based on admin/not admin
  const { keycloak, initialized } = useKeycloak();

  //access to redux state to edit and use the currently selected request
  const requestInfoState = useSelector((state) => state.viewRequest);

  //local state for showing request editing errors
  const [editRequestError, setEditRequestError] = useState(null);

  //access to redux GUI-RELATED state for the current request
  const requestInfoGuiState = useSelector((state) => state.viewRequestGui);

  //event handler for submitting changes to a request
  const onSubmit = async (requestTitle, selection) => {
    const token = keycloak.token;
    const [updatedRequestObject, error] = await updateRequestDetails(
      token,
      requestId,
      selection,
      requestTitle
    );
    if (updatedRequestObject) {
      updateRequestInfoState(updatedRequestObject);
      toggleModalWindow();
    } else {
      setEditRequestError(error);
    }
  };

  return (
    <div>
      <Modal show={modalIsOpen} size="xl">
        <Modal.Header closeButton onHide={toggleModalWindow}>
          <Modal.Title>Edit request</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <BaseRequestForm
            submitHandler={onSubmit}
            calendarTitle="Select new dates"
            formTitle="Information about your request"
            submitButtonName="Save changes"
            titleDefaultValue={requestInfoState.title}
            commentDefaultValue={
              requestInfoGuiState.comments[0].message
                ? requestInfoGuiState.comments[0].message
                : null
            }
            defaultSelectionStart={requestInfoState.periodStart}
            defaultSelectionEnd={requestInfoState.periodEnd}
            includesComments={false}
            height={500}
          ></BaseRequestForm>
        </Modal.Body>
        <Modal.Footer>
          {editRequestError}
          <button
            className="btn-logo btn-sep btn-logo-logo icon-close"
            variant="secondary"
            onClick={toggleModalWindow}
          >
            Close
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default EditRequestForm;
