import { useKeycloak } from "@react-keycloak/web";
import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css"; //necessary CSS to actually display a modal window
import "../../details.css";
import { getRequestById } from "../../api/request";
import { useSelector } from "react-redux";
import parrot from "../../parrot.gif";
import { REQUEST_STATUS_APPROVED } from "../../const/requestStatus";
import { updateNotificationContentStatus } from "../../api/notifications";
import {
  notificationTypeRequest,
} from "../../const/notificationTypes";

const RequestDetails = ({ requestId, updateRequestInfoState }) => {
  //keycloak access to conditionally render based on admin/not admin
  const { keycloak, initialized } = useKeycloak();

  //local state for showing reqeust editing errors
  const [loadRequestInfoError, setLoadRequestInfoError] = useState(null);

  //access to redux GUI-RELATED state for the current request
  const requestInfoGuiState = useSelector((state) => state.viewRequestGui);

  //useeffect set up to run once to get request by id on initial mount
  useEffect(() => {
    handleLoadRequestInfo();
  }, []);

  //load request info from api based on url param id
  const handleLoadRequestInfo = async () => {
    // get token
    const token = keycloak.token;
    const [requestObjectFromApi, error] = await getRequestById(
      token,
      requestId
    );
    if (requestObjectFromApi) {
      await updateRequestInfoState(requestObjectFromApi);
      //patch api call to backend that sets seen:true for this request and its comments - to avoid making unneeded notifications
      const token = keycloak.token;
      const [updateStatusResponse, error] =
        await updateNotificationContentStatus(
          token,
          notificationTypeRequest,
          requestId,
          true
        );
    } else {
      setLoadRequestInfoError(error);
    }
  };

  return (
    <div>
      <div>
        {loadRequestInfoError}
        {/* render details if requestinfo is not null */}
        {requestInfoGuiState && (
          <div className="dv-details">
            <div className="dv-details-info">
              <h3>"{requestInfoGuiState.title}"</h3>
              <b>Requested by: </b>
              {`${requestInfoGuiState.user.firstName} ${requestInfoGuiState.user.lastName}`}
              <br />
              <b>Vacation Start: </b>
              {requestInfoGuiState.periodStart}
              <br />
              <b>Vacation End: </b>
              {requestInfoGuiState.periodEnd}
              <br />
              <b>Request Status: </b>
              {requestInfoGuiState.requestStatus}{" "}
              {requestInfoGuiState.requestStatus ===
                REQUEST_STATUS_APPROVED && (
                <img src={parrot} height="30" width="30" />
              )}
            </div>
            {requestInfoGuiState.comments && (
              <div className="dv-details-comments">
                <h3>Comments</h3>
                {requestInfoGuiState.comments.map((comment) => {
                  return (
                    <div class="list-group">
                      <a
                        href="#"
                        className="list-group-item list-group-item-action w-90 flex-column align-items-start"
                      >
                        <div className="d-flex w-100 justify-content-between">
                          <h5 className="mb-1"></h5>
                          <small>{comment.timeStamp}</small>
                        </div>
                        <p id="comment-msg" className="mb-1">
                          {comment.message}
                        </p>
                        <sm>
                          Author: {comment.user.firstName}{" "}
                          {comment.user.lastName}
                        </sm>
                      </a>
                    </div>
                  );
                })}
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default RequestDetails;
