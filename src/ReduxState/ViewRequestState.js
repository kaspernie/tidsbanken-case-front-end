//state holding info about the request being viewed in ViewRequestView

//actions and exporting functions that expose them as objects
export const ACTION_VIEWREQUEST_SET_REQUEST_INFO =
  "[viewRequest] set request info";

export const setRequestObjectInfo = (requestObject) => {
  return { type: ACTION_VIEWREQUEST_SET_REQUEST_INFO, payload: requestObject };
};

//reducer for above actions
export const viewRequestReducer = (state = null, action) => {
  switch (action.type) {
    case ACTION_VIEWREQUEST_SET_REQUEST_INFO:
      return action.payload;
    default:
      return state;
  }
};
