import { fetchKeycloakAdminToken } from "./adminArea.js";

// keycloak
const keycloakAdminResourceUrl =
  process.env.REACT_APP_KEYCLOAK_ADMIN_API_RESOURCE_URL;

const adminGroupId = process.env.REACT_APP_KEYCLOAK_ADMIN_GROUP_ID;

// patch keycloak user to keycloak
export const updateKeycloakProfile = async (userObject, isAdmin, idUser) => {
  try {
    const [token, userError] = await fetchKeycloakAdminToken();
    const authString = `Bearer ${token.access_token}`;
    const response = await fetch(
      `${keycloakAdminResourceUrl}/users/${idUser}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: authString,
        },
        body: JSON.stringify({
          enabled: true,
          ...userObject,
        }),
      }
    );
    if (!response.ok) {
      throw new Error("Could not add user, status code is" + response.status);
    }
    const adminResponse = await updateKeycloakAdminStatus(isAdmin, idUser)
    return [response, null];
  } catch (error) {
    return [null, error.message];
  }
};

// update a user's keycloak groups to be admin/not admin
export const updateKeycloakAdminStatus = async (isAdmin, idUser) => {
  try {
    const [token, userError] = await fetchKeycloakAdminToken();
    const authString = `Bearer ${token.access_token}`;
    const response = await fetch(
      `${keycloakAdminResourceUrl}/users/${idUser}/groups/9eb757d5-b0c0-4a4e-b636-dc3a58a4d3a6`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: authString,
        },
        body: JSON.stringify({
          realm: "kc-react",
          userId: idUser, 
          groupId: adminGroupId
        }),
      }
    );
    if (!response.ok) {
      throw new Error("Could not add user, status code is" + response.status);
    }
    return [response, null];
  } catch (error) {
    return [null, error.message];
  }
};

// patch keycloak password to keycloak
export const resetKeycloakProfilePassword = async (userObject, idUser) => {
  try {
    const [token, userError] = await fetchKeycloakAdminToken();
    const authString = `Bearer ${token.access_token}`;
    const response = await fetch(
      `${keycloakAdminResourceUrl}/users/${idUser}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: authString,
        },
        body: JSON.stringify({
          enabled: true,
          ...userObject,
        }),
      }
    );
    if (!response.ok) {
      throw new Error("Could not add user, status code is" + response.status);
    }
    return [response, null];
  } catch (error) {
    return [null, error.message];
  }
};

// backend
const backendUrl = process.env.REACT_APP_BE_URL;

// Patch existing BE DB user
export const updateBackendProfile = async (userObject, idUser, token) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/user/${idUser}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
      body: JSON.stringify({
        email: userObject.email,
        firstName: userObject.firstName,
        lastName: userObject.lastName,
      }),
    });
    if (!response.ok) {
      throw new Error("Could not patch user, status code is" + response.status);
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

//logout to patch backend profile with new timestamp
export const updateLogoutTimestamp = async (timestamp, idUser, token) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/user/${idUser}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
      body: JSON.stringify({
        lastLogin : timestamp
      }),
    });
    if (!response.ok) {
      throw new Error("Could not patch user, status code is" + response.status);
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};