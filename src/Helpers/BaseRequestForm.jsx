import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid"; // plugin for dayGridMonth view
import interactionPlugin from "@fullcalendar/interaction"; // plugin for selectable
import { useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import "../forms.css";

const BaseRequestForm = ({
  submitHandler,
  calendarTitle,
  formTitle,
  submitButtonName,
  titleDefaultValue,
  commentDefaultValue,
  defaultSelectionStart,
  defaultSelectionEnd,
  includesComments,
  height,
}) => {
  //local state storing the current calender selection
  const [selection, setSelection] = useState(null);
  //handler for when user selects a section of the calendar
  const handleSelectDates = (selectionInfo) => {
    setSelection(selectionInfo);
  };

  //reference to calendar object to call methods on it.
  const calendarRef = React.createRef();

  //get ineligibleperiods using redux
  const ineligibleListState = useSelector((state) => state.ineligiblePeriods);
  useEffect(() => {
    if (defaultSelectionStart && defaultSelectionEnd) {
      const calendarApi = calendarRef.current.getApi();
      calendarApi.select(defaultSelectionStart, defaultSelectionEnd);
    }
  }, []);

  //disallows selecting dates before the current date
  const checkIfBeforeToday = (selectionInfo) => {
    const currentDay = new Date().setHours(0, 0, 0, 0);
    return +selectionInfo.start >= +currentDay ? true : false;
  };

  //disallow requests when period is ineligible and before today
  const checkIfAllowed = (selectionInfo) => {
    const isOutside = (ineligiblePeriod) =>
      ineligiblePeriod.end <= +selectionInfo.start ||
      ineligiblePeriod.start >= +selectionInfo.end;

    const checkLength = (selectionInfo, maxLength) => {
      let vacationDays = 0;
      let date = selectionInfo.start;
      while(date < selectionInfo.end){
        if(0<date.getDay() && date.getDay()<6){
          vacationDays ++;
        }
        date.setDate(date.getDate() + 1 );

      }
      return vacationDays<=maxLength;
    }
    if (
      ineligibleListState.every(isOutside) &&
      checkIfBeforeToday(selectionInfo) &&
        checkLength(selectionInfo, 4)
    ) {
      return true;
    }
    return false;
  };

  //using function from react hook form so we can handle the form in an SPA way without reloading page on submit
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({ mode: "onChange" });

  //rules for the input form for title and comment.
  const requestTitleConfig = {
    required: true,
    maxLength: 40,
  };

  const requestCommentConfig = {
    required: true,
    maxLength: 250,
  };

  //calls external submit handler function from parent component with the information input by user
  const onSubmit = ({ requestTitle, requestComment }) => {
    if (includesComments) {
      submitHandler(requestTitle, requestComment, selection);
    } else {
      submitHandler(requestTitle, selection);
    }
  };

  return (
    <div className="dv-requestform">
      <div className={"dv-requestform-calendar"}>
      <h4 className={"requestcalendar-title"}>{calendarTitle}</h4>
      <FullCalendar
        headerToolbar={{ left: "title", center: "", right: "prev,next" }}
        ref={calendarRef}
        //remember to register all plugins here
        plugins={[dayGridPlugin, interactionPlugin]}
        initialView="dayGridMonth"
        displayEventTime={false}
        events={ineligibleListState}
        contentHeight={height}
        //allows selecting sections
        selectable={true}
        //what to do when a section is selected as vacation request
        select={handleSelectDates}
        //clicking elsewhere does not deselect in GUI
        unselectAuto={false}
        //disallows selecting dates that are before the current date
        selectAllow={checkIfAllowed}
      />
      </div>
      <div className={"dv-requestform-form"}>
        <h4 className={"requestform-title"}>{formTitle}</h4>
        <h5 className={"request-dates-title"}>Dates selected: </h5>
          <p className="p-dates">
          {selection && (
              [selection.start.toLocaleString('dk-DK',{ weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }),
                " to ",
                new Date(selection.end-1).toLocaleString('dk-DK',{ weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })])}
          </p>

        <form onSubmit={handleSubmit(onSubmit)}>
          <fieldset>
            <label htmlFor="requestTitle">Request title:</label>
            <input
              className="form-control"
              type="text"
              {...register("requestTitle", requestTitleConfig)}
              placeholder="Limit 40 characters"
              defaultValue={titleDefaultValue}
            />
          </fieldset>
          {includesComments && (
            <fieldset>
              <label htmlFor="requestComment">Request comment:</label>
              <textarea
                className="form-control"
                {...register("requestComment", requestCommentConfig)}
                placeholder="Limit 250 characters"
                defaultValue={commentDefaultValue}
              />
            </fieldset>
          )}
          <button
              className={"btn-logo btn-sep btn-logo-logo icon-paperplane"}
            type="submit"
            disabled={!isValid || !selection}
          >
            {submitButtonName}
          </button>
        </form>
      </div>
    </div>
  );
};

export default BaseRequestForm;
